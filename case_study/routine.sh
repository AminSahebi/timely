#!/bin/bash

header="+++++++++++++++++\n++ Test routine to report user time and kernel time\n++ of a MPI program, here Matrix Multiply and Fibonacci\n++ as case study\n"

echo -e $header


echo "compilation"

OMPIV=`mpirun.openmpi --version`
OMPICMD==`mpicc.openmpi`


MPICHV=`mpirun.mpich --version`
MPICHCMD=`mpicc.mpich`


