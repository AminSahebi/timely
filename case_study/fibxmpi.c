#include <stdio.h>
#include <mpi.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
//#define _DEBUG_

int N=5;
int thd=1;
#define RECFIB

#define XDATA_INT64

#ifdef XDATA_SINGLE
    #define DATA float
    #define MPI_DATATYPE MPI_FLOAT
#endif
#ifdef XDATA_DOUBLE
    #define DATA double
    #define MPI_DATATYPE MPI_DOUBLE
#endif
#ifdef XDATA_INT64
    #define DATA uint64_t
    #define MPI_DATATYPE MPI_UINT64_T
#endif

int call_counts,slave_counts, master_counts;

struct rusage start_usage,end_usage;
struct timespec start_time,end_time,elapsed, user,kernel;


void prepare();
void compute(char **argv);
void report();


//global variables

DATA fibn;
int32_t universe_size;
int32_t myrank; 
char command[255];
int32_t world_size, flag;
int32_t flag_master;

int spawn_cnt,call_cnt;
//MPI environments variables
MPI_Info local_info;
MPI_Status status;
MPI_Request send;
MPI_Comm parent;

char name[MPI_MAX_PROCESSOR_NAME];
int len_name;



long serialfib(int ser_n)
{
    #ifdef RECFIB

		call_counts+=1;
		if(ser_n <= 1) return 1;
		else{
			return serialfib(ser_n-1) + serialfib(ser_n-2);}
    #else
	int a=-1,b=1;
	int i=-2;
	for(; i<ser_n;++i)
	{   
	    int z=a+b;
	    a=b;
	    b=z;
	}
	return a;
	#endif
}




void report()
{
	
	printf("[INFO]: START report()\n");
	#ifdef _DEBUG_
		printf("Fibonacci result: %ld\n",fibn);
	#endif

	DATA superchecksum = serialfib(N);
	printf("Fibonacci Check: %ld\n",superchecksum);

	if(fibn = superchecksum)
	{
		printf("*** %s ***\n","SUCCESS");
		printf("All workers done, goodbye\n");
	}
	else
	{
		printf("*** %s ***\n","FAILURE");
	}

	    //getrusage(RUSAGE_SELF, &end_usage);
                user.tv_sec = end_usage.ru_utime.tv_sec - start_usage.ru_utime.tv_sec;
                user.tv_nsec = 1000*(end_usage.ru_utime.tv_usec - start_usage.ru_utime.tv_usec);

                kernel.tv_sec = end_usage.ru_stime.tv_sec - start_usage.ru_stime.tv_sec;
                kernel.tv_nsec = 1000*(end_usage.ru_stime.tv_usec - start_usage.ru_stime.tv_usec);


                printf("%10lu.%09lu s user time\n",user.tv_sec,user.tv_nsec);
                printf("%10lu.%09lu s kernel time\n",kernel.tv_sec,kernel.tv_nsec);

	printf("[INFO]: END report()\n");
	
	fflush(stdout);
}



void prepare()
{

	MPI_Comm_size (MPI_COMM_WORLD, &world_size);
	if (world_size != 1)
	{
		perror ("Top heavy with management");
	exit(0);
	}
	MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
	MPI_Info_create (&local_info);
	MPI_Comm_get_parent(&parent);
	if (parent == MPI_COMM_NULL)
		flag_master=1;
		
	universe_size = 5000;
	
	MPI_Comm_get_attr (MPI_COMM_WORLD, MPI_UNIVERSE_SIZE, &universe_size,&flag);
	
	if (universe_size == 1)
	{	
		perror ("No room to start workers");
		exit(0);
	}
	
	
	#ifdef _DEBUG_
		MPI_Get_processor_name(name,&len_name);
		printf("Runnuning on node: %s\n",name);
	#endif

}



void  master_fib(char **argv)
{

	MPI_Comm master_comm;
	
	DATA n_master=N;
	sprintf(command,"%s",argv[0]);
	
	argv+=1;	
		
	if (n_master < thd ){
		#ifdef _DEBUG_
		//	printf("MASTER: threashold reached: %ld - %d\n",n,thd);
		#endif
		fibn = serialfib(n_master);    
	}else{
		sprintf(argv[0],"%ld",n_master);
		#ifdef _DEBUG_
			printf ("<root> spawning recursive process, n = %ld\n", atol(argv[0]));
		#endif
		MPI_Comm_spawn (command, argv, 1, local_info, myrank, MPI_COMM_SELF,
                      &master_comm, MPI_ERRCODES_IGNORE); 
	MPI_Recv (&fibn, 1, MPI_LONG, MPI_ANY_SOURCE, 1, master_comm,
             MPI_STATUS_IGNORE);

	MPI_Comm_disconnect(&master_comm);
	}
}



void slave_fib(char **argv)
{
	MPI_Comm children_comm[2];
	MPI_Comm_get_parent(&parent);	
	DATA x,y,n_slave;
	sprintf(command,"%s",argv[0]);
	argv += 1;
	n_slave  = atol (argv[0]);
	if (n_slave < thd || n_slave <2)
	{
		#ifdef _DEBUG_
			printf ("threshold reached: %ld - %d\n",n_slave,thd);
		#endif
		DATA res = serialfib(n_slave);
		MPI_Isend (&res, 1, MPI_DATATYPE, 0, 1, parent,&send);
		MPI_Wait(&send,&status);
//		MPI_Send (&res, 0, MPI_LONG, 0, 1, parent);
	}else{
		#ifdef _DEBUG_
			printf ("<%ld> spawning new process (1)\n", n_slave);
		#endif
		sprintf (argv[0], "%ld", (n_slave - 1));

		MPI_Comm_spawn (command, argv, 1, local_info, myrank,
				MPI_COMM_SELF, &children_comm[0], MPI_ERRCODES_IGNORE);
		#ifdef _DEBUG_
			printf ("<%ld> spawning new process (2)\n", n_slave);
		#endif

		sprintf (argv[0], "%ld", (n_slave - 2));

		MPI_Comm_spawn (command, argv, 1, local_info, myrank,
                     MPI_COMM_SELF, &children_comm[1], MPI_ERRCODES_IGNORE);
 
		MPI_Recv (&x, 1, MPI_DATATYPE, MPI_ANY_SOURCE, 1,
               children_comm[0], MPI_STATUS_IGNORE);
     
		MPI_Recv (&y, 1, MPI_DATATYPE, MPI_ANY_SOURCE, 1,
                children_comm[1],MPI_STATUS_IGNORE);
   
		fibn = x + y;             // computation

		MPI_Isend (&fibn, 1, MPI_DATATYPE, 0, 1, parent,&send);
		MPI_Wait(&send,&status);
//		MPI_Send (&fibn, 0, MPI_LONG, 0, 1, parent);
	}
	MPI_Comm_disconnect(&parent);
}


void compute(char **argv){
	
  
	if (flag_master)
	{
		master_fib(argv);
	}else
	{
		slave_fib(argv);
	}
}

int main (int argc, char **argv)
{
	
	MPI_Init (&argc, &argv);
	N = atoi(argv[1]);
	thd = atoi(argv[2]);
	struct timeval tv;
	long long time_in_mill, start_time;
	flag_master=0;
	prepare();
	if(flag_master)
	{	
	printf("[INFO]: END PREPARE\n");
	printf("[INFO]: SART COMPUTE: fib of %d with thd %d\n",N,thd);
	
       	getrusage(RUSAGE_SELF, &start_usage);
	}

	
	compute(argv);
    
	if(flag_master)
	{
    
       	getrusage(RUSAGE_SELF, &end_usage);

		printf("[INFO]: END COMPUTE\n");
	
		report();
	}
	MPI_Finalize();
	return 0;
}



