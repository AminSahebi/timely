#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <sys/resource.h>
#include <time.h>

struct rusage start_usage,end_usage;
struct timespec start_time,end_time,elapsed, user,kernel;

FILE *fd;
double *mem;
int main(int argc, char *argv[]) {
	struct timespec start,stop;
	int size =100000000;
	
	clock_gettime(CLOCK_REALTIME, &start);
	getrusage(RUSAGE_SELF, &start_usage);
	//long long start=get_time();
	mem = (double*) malloc(size*sizeof(double));

	fd = fopen("log","w");

	if(fd == NULL)
	{
		printf("Error!");   
		exit(1);             
	}


	for (int i = 0; i< size ; i++){
		mem[i] = rand()%10;
		fprintf(fd,"%f\n",mem[i]);
	}

	clock_gettime(CLOCK_REALTIME, &stop);
	getrusage(RUSAGE_SELF, &end_usage);
	//long long stop = get_time();
	user.tv_sec = end_usage.ru_utime.tv_sec - start_usage.ru_utime.tv_sec;
	user.tv_nsec = 1000*(end_usage.ru_utime.tv_usec - start_usage.ru_utime.tv_usec);

	kernel.tv_sec = end_usage.ru_stime.tv_sec - start_usage.ru_stime.tv_sec;
	kernel.tv_nsec = 1000*(end_usage.ru_stime.tv_usec - start_usage.ru_stime.tv_usec);
	
	
	printf("\t%.2lu.%.9lu s user time\n",user.tv_sec,user.tv_nsec);
	printf("\t%.2lu.%.9lu s kernel time\n",kernel.tv_sec,kernel.tv_nsec);
	printf("\t%.2lu.%.9lu s runtime\n",stop.tv_sec - start.tv_sec, (stop.tv_nsec - start.tv_nsec));

	fclose(fd);
	return 0;
}
