#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#define RANDLIMIT	5	/* Magnitude limit of generated randno.*/
#define N		400  	/* Matrix Size */
#define NUMLIMIT 70.0

float a[N][N];
float b[N][N];
float c[N][N];

uint64_t get_cpu_freq(void) {
	FILE *fd;
	uint64_t freq = 0;
	float freqf = 0;
	char *line = NULL;
	size_t len = 0;

	fd = fopen("/proc/cpuinfo", "r");
	if (!fd) {
		fprintf(stderr, "failed to get cpu frequency\n");
		perror(NULL);
		return freq;
	}

	while (getline(&line, &len, fd) != EOF) {
		if (sscanf(line, "cpu MHz\t: %f", &freqf) == 1) {
			freqf = freqf * 1000000UL;
			freq = (uint64_t) freqf;
			break;
		}
	}

	fclose(fd);
	return freq;
}

void matrixMultiply(){
	int i, j, k;


	/* generate mxs  */
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++) {
			a[i][j] = 1 + (int)(NUMLIMIT*rand() / (RAND_MAX + 1.0));
			b[i][j] = (double)(rand() % RANDLIMIT);

		}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			c[i][j] = 0.0;
			for (k = 0; k < N; k++)
				c[i][j] = c[i][j] + a[i][k] * b[k][j];
		} /* end j loop */
	}
}



int main(){
	int i=0;
	int pid = getpid();
	int parentsPID = getppid();
	FILE *fd1;
	FILE *fd2;
	fd1 = fopen("pid","w");
	//while(i < 10){
	//i++;
	matrixMultiply();
	//	}
	fprintf(fd1,"%d\n",pid);
	fprintf(fd1,"%2.2f\n",get_cpu_freq()/1e6);
	fclose(fd1);
	return 0;
}
