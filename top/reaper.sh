#!/bin/sh
#echo $$ >> $1.pid.log
#cat /proc/$$/stat >> $1.pid.log
#exec "$@"

file="ps.log"

uptime=$(cat /proc/uptime | awk '{print $1}')
uptime=${uptime%.*}
pid= $$
ticks=$(getconf CLK_TCK)
u=$(cat /proc/$pid/stat | awk '{print $14}')
s=$(cat /proc/$pid/stat | awk '{print $15}')
cu=$(cat /proc/$pid/stat | awk '{print $16}')
cs=$(cat /proc/$pid/stat | awk '{print $17}')
starttime=$(cat /proc/$pid/stat | awk '{print $22}')

totaltime=$(($u+$s+$cu+$cs))
st=$((starttime/ticks))
seconds=$(($uptime - $st))	
tt=$((totaltime/ticks))
tts=$(echo "scale=4; (($tt/$seconds))" | bc -l | awk '{printf "%f", $0}')
cpu=$(echo "scale=4; (($tts * 100))" | bc -l | awk '{printf "%f", $0}')


echo "  $pid \
	$uptime \
	$ticks \
	$tt \
	$tts \
	$cpu"
