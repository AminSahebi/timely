#include <time.h>
#include <stdio.h>
#include <sys/time.h>

int main()
{
	printf("[INFO] First method\n");
	struct timeval tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10;
	struct timespec ts1, ts2, ts3, ts4, ts5, ts6, ts7, ts8, ts9, ts10;
	int i=1;
	int ts=0;
	int tv=0;
	int max = 100000;
	int itr = 10;
	while (i++ < max) {
		/* run multiple times each method and measure the time */
		gettimeofday(&tv1, NULL);
		gettimeofday(&tv2, NULL);
		gettimeofday(&tv3, NULL);
		gettimeofday(&tv4, NULL);
		gettimeofday(&tv5, NULL);
		gettimeofday(&tv6, NULL);
		gettimeofday(&tv7, NULL);
		gettimeofday(&tv8, NULL);
		gettimeofday(&tv9, NULL);
		gettimeofday(&tv10, NULL);


		clock_gettime(CLOCK_REALTIME, &ts1);
		clock_gettime(CLOCK_REALTIME, &ts2);
		clock_gettime(CLOCK_REALTIME, &ts3);
		clock_gettime(CLOCK_REALTIME, &ts4);
		clock_gettime(CLOCK_REALTIME, &ts5);
		clock_gettime(CLOCK_REALTIME, &ts6);
		clock_gettime(CLOCK_REALTIME, &ts7);
		clock_gettime(CLOCK_REALTIME, &ts8);
		clock_gettime(CLOCK_REALTIME, &ts9);
		clock_gettime(CLOCK_REALTIME, &ts10);
	
	
		if ((ts10.tv_nsec - ts1.tv_nsec) /1000.0 > ((tv10.tv_usec - tv1.tv_usec))) {
			++tv; //add down score to tv
		} else if ((ts10.tv_nsec - ts1.tv_nsec)/1000.0 < ((tv10.tv_usec - tv1.tv_usec))){
			++ts; //add down score to ts
		}
	}
	printf("\n");
	printf("[INFO] running %d times the ts (timespec)  and tv (timeval) libraries consequently\n",max*itr);
	printf("[INFO] then measure the deviation created by each library.\n\n");
	printf("[INFO] ts timespec (clock_gettime) -> %d\n", ts);
	printf("[INFO] tv timeval (gettimeofday)   -> %d\n", tv);
	if (ts > tv)
		printf("[INFO] ts > tv , tv is %.2f%% faster\n",(tv*1.0/ts)*100);
	else
		printf("[INFO] ts < tv , ts is %.2f%% faster\n",(ts*1.0/tv)*100);

	return 0;
}
