#!/bin/bash

if [ "$2" = "" ]; then echo "You must specify matrix size, number of workers"; exit 1; fi

i=$1
k=$2
set -x

mpirun -n $k --hostfile ~/hosts ./bmm-mpi $i

