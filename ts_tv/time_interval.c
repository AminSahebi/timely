#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

static double *a; /* input matrix */
static double *b; /* input matrix */
static double *c; /* result matrix */
extern inline double get_timeday();
extern inline double get_clocktime();

inline double get_timeday() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec + (tv.tv_usec / 1e6);
}

inline double get_clocktime() {
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	return ts.tv_sec + (ts.tv_nsec / 1e9);
}

void mmul(int N){
	int i,j,k;
	double t1, t2;

	a = (double *)malloc(N*N*sizeof(double));
	b = (double *)malloc(N*N*sizeof(double));
	c = (double *)malloc(N*N*sizeof(double));
	for(i=0; i<N; i++)
	{
		for(j=0; j<N; j++)
		{
			a[i*N+j] = (double)i;
			b[i*N+j] = (double)j;
		}
	}
	for(i=0; i<N; i++)
	{
		for(j=0; j<N; j++)
		{
			c[i*N+j] = 0.0;
			for(k=0; k<N; k++)
				c[i*N+j] = c[i*N+j] + a[i*N+k]*b[k*N+j]; /* most time spent here! */
		}
	}
	free(a);
	free(b);
	free(c);
} /* end time_matmul.c */
/*
void compute_1(int iter){
	printf("mmul in %d iterations\n",iter);
	time_t startTime;
	int iteration = 0;
	double start = get_timeday();
	while (iteration < iter)	
	{
		mmul (500);
		iteration++;

	}
		printf("gettimeofday iteration %d finished %f\n",iteration, (get_timeday() - start)/iter);
}
*/
void compute_2(int iter){
	printf("mmul in %d iterations\n",iter);
	time_t startTime;
	int iteration = 0;
	double start = get_timeday();
	while (iteration < iter)	
	{
		mmul (500);
		iteration++;

	}
		printf("clock gettime %d finished %f\n",iteration, (get_clocktime() - start)/iter);
}

int main(int argc, char ** argv){
	setbuf(stdout, NULL); // Do not buffer prints

	int iter =1000;

//	compute_1 (iter);
	compute_2 (iter);
return 0;
}



