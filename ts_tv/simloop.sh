#!/bin/bash
mode="tv"
host=`hostname -s`
release=`lsb_release -r | awk 'NR==1{printf $2}' | cut -c -2`
distro=`lsb_release -d  | awk 'NR==1{print $2.$3}' | cut -c -1`
date=`date +%Y%m%d%H%M | cut -c 3-`
#core=`lscpu | awk 'NR==4{print "C"$2}'`
core=`getconf _NPROCESSORS_ONLN`
#echo "$distro"
#echo "$date"
#wdir="$HOME/XOCR/ocr" # working dir
wdir=`pwd`
toolname=`pwd | awk -F/ '{print $5$6}'`
cpath="run_bmm.sh" #command path
size="1728" # input range
irange="1 3 6 9 12 15 18 21 24 27 30 33 36 39 42 45 48"
#irange="3 4 5"
mstring="EXECUTION TIME" # matching string
#mstring="time elapsed" # matching string
mcolumn="5" # matching column
nrep="10" #number of repetition of the test
#-----------------------------------
if [ ! -d "$wdir" ]; then
	echo "ERROR: cannot find '$wdir'"; exit 1
fi

if [ ! -s "$wdir/$cpath" ]; then
	echo "ERROR: cannot find '$cpath' in '$wdir'"; exit 1
fi

cd $wdir
FILENAME=$toolname-$host$distro$release"C"$core-$date-$mode
re='^[0-9]+$'
echo "pwd=`pwd`"
echo "create log and plot file name: $FILENAME.csv"
echo "logs and plots will store into $wdir/logs/"
echo ""
declare -a value
#printf %s'\n' "n,avg,min,max" > $FILENAME.csv
for i in $irange; do  
	a="0"; min=""; max="0"
	for r in `seq $nrep`; do

		output=`./$cpath $size $i  2>error.log`
		val=`echo "$output"|awk "/$mstring/{print \\$5}" c=$mcolumn`
		if ! [[ $val =~ $re ]] ; then echo "ERROR: The output is not a number! (input=$i,output='$val')" >&2; exit 1; fi
		if [ 1 = `echo "$val > $max"|bc` ]; then max="$val"; fi
		if [ "$min" = "" ]; then 
			min="$val"; a="$val"
		else
			if [ 1 = `echo "$val < $min"|bc` ]; then min="$val"; fi
		fi
		i1=`expr $i - 1`
		a=`echo "define trunc(x) { auto s; s=scale; scale=0; x=x/1; scale=s; return x } trunc($a * $i1 / $i + $val / $i)"|bc -l`
		if [ "$i1" = "0" ]; then
                        value="$a"
                fi
                b=`echo "define trunc(x) { scale=2; x=x/1.0;  return x } trunc($value/$a)"|bc -l`
                #b=$(echo "$value/$a" | bc -l)
		#      echo "$i -- $val -- $a -- $min -- $max"

	done
	echo "$i,$a,$min,$max,$value,$b"
	printf %s'\n' "$i,$a,$min,$max" >> $FILENAME.csv
done


for FILE in ${FILENAME}.csv; do
	gnuplot <<- EOF
	set xlabel "Number of workers"
	set ylabel "Execution time (ms)"
	set key left
	set term png
	set style histogram cluster gap 1
	set style fill solid 0.5
	set boxwidth 0.9
	set style histogram errorbars linewidth 1
	set errorbars linecolor black
	red = "#FF0000"; green = "#00FF00"; blue = "#0000FF"; skyblue = "#87CEEB" ; violet = "#FF00FF"; purple = "#440154" ;
	set grid ytics
	set format y '10^{%L}' 
	set logscale y
	set autoscale x
	set yrange [1:]
	set output "${FILENAME}.png"
	set datafile separator ","
	set style data histogram
	plot "${FILENAME}.csv" using 2:3:4:xtic(1) title "Execution time (ms)" linecolor rgb purple linewidth 0
	EOF
done

if [ -d "$wdir/logs" ] 
then
	mkdir -p "$wdir/logs"
fi
cp ${FILENAME}.csv $wdir/logs
cp ${FILENAME}.png $wdir/logs

rm -f *.csv
rm -f *.png
